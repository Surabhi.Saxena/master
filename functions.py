import numpy as np
from PIL import Image


# function to pad given image with zero padding
# for the given filter of size filter_size
# padding size = filter_size//2
def pad_with_zero(img, filter_size):
    return np.pad(img, filter_size // 2, 'constant', constant_values=(0, 0))


# function to pad given image with zero padding
# for the given filter of size (filter_size_x , filter_size_y)
def pad_with_zeros(img, filter_size_x, filter_size_y):
    return np.pad(img, ((filter_size_x // 2 -1 ,filter_size_x // 2) , (filter_size_y // 2 -1, filter_size_y // 2 )), 'constant', constant_values=(0, 0))

# function to print table format
def pretty_print_table(data):
    for i, d in enumerate(data):
        line = '|'.join(str(x).ljust(12) for x in d)
        print(line)
        if i == 0:
            print('-' * len(line))

# function to crop the main image from padded image
def remove_padding(padded_image, M, N):
    '''
    :param padded_image:
    :param M:
    :param N:
    :return:
    '''
    return padded_image[M // 2 - 1: M // 2 + M - 1, N // 2 - 1:N // 2 + N - 1]


# function to convolve different filters with the image
def apply_filter(image, filter, padFunction):
    """
    apply square filter to an image with padding function provided
    :param image: greyscale image of with data range[0,255]
    :param filter: square filter represented in np array
    :param padFunction:  padding function to be used, defaults to zero padding
    :return: image of same shape as input with filtering applied.
    """
    filter_x, filter_y = filter.shape
    result = np.zeros(image.shape, dtype='float')
    assert filter_x == filter_y, "must be a square filter"
    pad_size = filter_x // 2
    image_p = padFunction(image, filter_x)
    # print("Padded image", image_p)
    image_x, image_y = image_p.shape
    # print("Loops:", pad_size,(image_x -   pad_size) )
    for i in range(pad_size, (image_x -   pad_size)):
        for j in range(pad_size, (image_y -  pad_size)):
            # print("Computing for ", i, j)
            w = compute_window(image_p, i, j, filter_x)
            # print(i,j,w.shape)
            # print ("Window ", w)
            val = np.sum(np.multiply(w, filter))
            # if val == 0 and i > 255 and j > 255:
            # print(i,j,w)
            # print ("Value ", val)
            result[i - pad_size][j - pad_size] = val
    return result


# function to compute nxn window from the image
# where x,y is central element of the window and nxn is the filter size
def compute_window(image, x, y, filter_size):
    pad_width = filter_size // 2
    return image[x - pad_width:x - pad_width + filter_size, y - pad_width:y - pad_width + filter_size]



def test_filter():
    test_image = np.array([[1, 8, 6, 1, 7],
                           [4, 1, 9, 3, 7],
                           [2, 6, 2, 2, 9],
                           [4, 7, 7, 1, 2],
                           [8, 3, 7, 4, 7]])
    filter = np.ones((3, 3), dtype='float')
    result = apply_filter(test_image, filter)
    print(result)

# Box filter
def get_box_filter(size):
    return np.ones((size, size)) / (size * size)

# Laplacian filter
def get_laplace_filter():
    return np.asarray([[0, 1, 0], [1, -4, 1], [0, 1, 0]])


# Gaussian filter
def get_gaussian_filter():
    return np.array([[1, 2, 1],  [2, 4, 2],  [1, 2, 1]]) / 16


# Ideal Lowpass Filter
def ideal_lowpass_filter(image,D_0):
    size_x, size_y = image.shape    # finding shape of image
    image_pad = pad_with_zeros(image, size_x, size_y)    # padding image

    image_pad_x , image_pad_y = image_pad.shape    # finding padded image shape
    x = np.linspace(-1* (image_pad_x//2), image_pad_x//2, image_pad_x)    # corr to meshgrid 'centering'
    y = np.linspace(-1* (image_pad_y//2), image_pad_y//2, image_pad_y)    # corr to meshgrid 'centering'
    u, v = np.meshgrid(x,y)

    z = np.sqrt(np.power(u,2) + np.power(v,2))      # finding distance. Not subtracting since we have already centered filter at (0,0)

    lp_filter = np.where(z > D_0 , 0 , 1)    # applying ideal lowpass filter definition. Choosing for corr D_0
    return lp_filter


# Butterworth Lowpass Filter
def butterworth_filter(image,D_0, n):
    size_x, size_y = image.shape    # finding shape of image
    image_pad = pad_with_zeros(image, size_x, size_y)    # padding image

    image_pad_x , image_pad_y = image_pad.shape    # finding padded image shape
    x = np.linspace(-1* (image_pad_x//2), image_pad_x//2, image_pad_x)    # corr to meshgrid centering
    y = np.linspace(-1* (image_pad_y//2), image_pad_y//2, image_pad_y)    # corr to meshgrid centering
    u, v = np.meshgrid(x,y)

    z = np.sqrt(np.power(u,2) + np.power(v,2))      # finding distance. Not subtracting since we have already centered filter at (0,0)
    filter_func = lambda  x : 1 / (1 + np.power(x/D_0,2*n))
    bw_filter = (np.vectorize(filter_func))(z)
    return bw_filter


# Gaussian Lowpass Filter
def gaussian_lowpass_filter(image, sigma):
    size_x, size_y = image.shape    # finding shape of image
    image_pad = pad_with_zeros(image, size_x, size_y)    # padding image

    image_pad_x , image_pad_y = image_pad.shape    # finding padded image shape
    x = np.linspace(-1* (image_pad_x//2), image_pad_x//2, image_pad_x)    # corr to meshgrid centering
    y = np.linspace(-1* (image_pad_y//2), image_pad_y//2, image_pad_y)    # corr to meshgrid centering
    u, v = np.meshgrid(x,y)

    z = np.sqrt(np.power(u,2) + np.power(v,2))      # finding distance. Not subtracting since we have already centered filter at (0,0)
    filter_func = lambda  x : np.exp(-1 * x**2 / (2*sigma**2))
    gaussian_filter = (np.vectorize(filter_func))(z)
    return gaussian_filter


# Ideal Highpass Filter
def ideal_highpass_filter(image, D_0):
    lp_filter = ideal_lowpass_filter(image, D_0)
    return 1 - lp_filter

