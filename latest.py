import imageio
import matplotlib.pyplot as plt
from skimage.color import rgb2gray
from numpy.linalg import inv
from functions import *
import numpy as np
import random
from skimage.metrics import mean_squared_error


fig=plt.figure()

cameraman_grey= rgb2gray(imageio.imread('cameraman.png').astype(float))       # function to find grey scale of image cameraman.png.
print("cameraman shape",cameraman_grey.shape)    # its (256,256)//

fig.add_subplot(1, 5, 1)
plt.imshow(cameraman_grey, cmap=plt.cm.gray)    # displaying original cameraman image
plt.title("Original image")
plt.axis('off')

# Synthesized Lowpass Filter
image_path = "lp_filter_new.png"
filter = rgb2gray(imageio.imread(image_path).astype(float))
#filter_new = filter[:256,:256]
#matplotlib.image.imsave('lp_filter_new.png', filter_new)

h, w = filter.shape     # filter shape is (256,256)
print("h,w are",h,w)


# --- iDFT ----
phase = np.zeros(filter.shape)
spectrum = np.multiply(filter, np.exp(1j * phase))
ifilter = np.abs(np.real(np.fft.ifft2(np.fft.ifftshift(spectrum), norm=None)))    # why w/o np.abs, it works///////

# --- symmetric filter synthesis---------------------------------
s = 128 # filter half size
f = ifilter[:s, :s]      # size is (128,128)
f_help = np.fliplr(f)[:, :-1]    # size is (128,127)
f = np.hstack((f_help, f))     # size is (128,255)

f_help = np.flipud(f)[:-1, :]
f = np.vstack((f_help, f))
syn_filter = f / np.sum(f)    # size is (255,255)
print("syn_filter shape is ",syn_filter.shape)

filter_pad = np.pad(syn_filter,((0,1),(0,1)), 'constant', constant_values=(0, 0))   # So, H = filter_pad
print('filter_pad shape',filter_pad.shape)   # its (256,256)

fig.add_subplot(1, 5, 2)
plt.imshow(filter_pad, cmap=plt.cm.gray)    # displaying Synthesized Lowpass Filter
plt.title("Syn Lowpass filter")
plt.axis('off')

fou = np.fft.fftshift(np.fft.fft2(cameraman_grey))    # FT of cameraman
print("shape is",fou.shape)    # shape is (256,256)
fou_fil = np.abs(np.fft.fftshift(np.fft.fft2(filter_pad)))    # FT of H(syn filter). Its shape is (256,256)
filtered_ft = np.multiply(fou, fou_fil)    # pointwise multiplication of padded cameraman with lowpass filter
blur_image = np.abs(np.real(np.fft.ifft2(filtered_ft)))    # finding inverse FT


fig.add_subplot(1, 5, 3)
plt.imshow(blur_image, cmap=plt.cm.gray)
plt.title("Blurred img by syn filter")
plt.axis('off')

fig.add_subplot(1, 5, 4)
plt.imshow(fou_fil, cmap=plt.cm.gray)
plt.title("FT of syn filter")
plt.axis('off')


# New part

# adding zero mean Gaussian noise to blurred_image i.e. Hf. So Hf + n is noisy_cameraman
mean = 0
std = 10
row, col = blur_image.shape
gauss_noise = np.random.normal(mean, std, (row, col))  # finding Gaussian dist by normalising sigma by 255 as image has values between 0 and 1
gauss_noise = gauss_noise.reshape(row, col)
noisy_cameraman = blur_image + gauss_noise     # adding noise to image
fig.add_subplot(1, 5, 5)
plt.imshow(noisy_cameraman, cmap=plt.cm.gray)      # displaying blur cameraman with zero mean Gaussian noise with std = 10
plt.title("Blur img + Gaussian noise")    # noisy cameraman
plt.axis('off')

plt.show()
plt.close()

max_pixel = 255    # corresponding to max[f(x,y)] = 255
def calculate_psnr(image1, image2):        # function to calculate PSNR between two images
    mse = mean_squared_error(image1, image2)        # calculating MSE between two images
    return 10 * np.log10(max_pixel**2 / mse)    # since PSNR = 10log10[max(f^2)/MSE]

psnr = calculate_psnr(noisy_cameraman, blur_image)
print("psnr between blur cameraman and noisy cameraman is",psnr)

#New part

# adding salt and pepper noise
def add_snp_noise(img, number_of_pixels):
    copy = np.copy(img)
    row , col = copy.shape     # Getting the dimensions of the image

    #number_of_pixels = random.randint(900, 1000)    # Randomly pick some pixels in the image for coloring them white. # Pick a random number between 900 and 1000
    # print(number_of_pixels)
    for i in range(number_of_pixels):
        y_coord=random.randint(0, row - 1)     # Pick a random y coordinate
        x_coord=random.randint(0, col - 1)     # Pick a random x coordinate

        copy[y_coord][x_coord] = 255            # Color that pixel to white


    #number_of_pixels = random.randint(900 , 1000)    # Randomly pick some pixels in the image for coloring them black.Pick a random number between 900 and 1000
    for i in range(number_of_pixels):
        y_coord=random.randint(0, row - 1)      # Pick a random y coordinate
        x_coord=random.randint(0, col - 1)      # Pick a random x coordinate

        copy[y_coord][x_coord] = 0              # Color that pixel to black

    print ("Difference is >" , np.sum(copy-img))
    return copy

fig=plt.figure()

img1=add_snp_noise(noisy_cameraman, 6553)      # Confirm with olga. Whether to apply it on blur cameraman or noisy cameraman
img2=add_snp_noise(noisy_cameraman, 16384)     # same
img3=add_snp_noise(noisy_cameraman, 22937)     # same


fig.add_subplot(1, 4, 1)
plt.imshow(noisy_cameraman, cmap=plt.cm.gray)      # displaying blur cameraman corrupted 20%
plt.title("Noisy cameraman image")
plt.axis('off')

fig.add_subplot(1, 4, 2)
plt.imshow(img1, cmap=plt.cm.gray)      # displaying blur cameraman corrupted 50%
plt.title("Noisy cameraman image crpt by 20%")
plt.axis('off')

fig.add_subplot(1, 4, 3)
plt.imshow(img2, cmap=plt.cm.gray)      # displaying blur cameraman corrupted 70%
plt.title("Noisy cameraman image crpt by 50%")
plt.axis('off')

fig.add_subplot(1, 4, 4)
plt.imshow(img3, cmap=plt.cm.gray)      # displaying blur cameraman corrupted 20%
plt.title("Noisy cameraman image crpt by 70%")
plt.axis('off')
plt.show()

hp_filt_kernel = np.array([[-1, -1, -1, -1, -1],
                   [-1,  1,  2,  1, -1],
                   [-1,  2,  4,  2, -1],
                   [-1,  1,  2,  1, -1],
                   [-1, -1, -1, -1, -1]])       # high pass 5*5 filter. sum = 0

hp_filter_x, hp_filter_y = hp_filt_kernel.shape

def zhi(r):
    # r/(1+(r/theta)**2 nu
    # r is the image here
    n, m = r.shape     # n = m = 256
    identity = np.identity(n)
    theta = 35
    nu = 5
    return np.multiply(r, inv(identity + np.power(r / theta, 2 * nu)))     # multiplication element wise

def grad_func(g, w):      # w is iterating image
    # -2* H.T * zhi(g - H.w) + 2 alpha C.T C w
    alpha = 0.01
    ctc = np.transpose(hp_filt_kernel) * hp_filt_kernel

    fou1 = np.fft.fftshift(np.fft.fft2(w))  # FT of w. From previous, we have fou_fil = FT of H
    filtered_ft1 = np.multiply(fou_fil,fou1)  # pointwise multiplication of w with H in Fourier domain
    k = np.abs(np.real(np.fft.ifft2(filtered_ft1)))  # finding inverse FT
    result1=  zhi(g - k)

    ctc_w = apply_filter(w, ctc, pad_with_zero)    # ctc_w shape is (256,256)

    ht_result1 = apply_filter(result1, np.transpose(filter_pad), pad_with_zero)     # ht_result1 shape is (256,256)

    return (-2 * ht_result1) + (2 * alpha * ctc_w)

# Make threshold a -ve value if you want to run exactly
# max_iterations.
mse = []
def gradient_descent(max_iterations, w_init, learning_rate):
    w = w_init
    i = 0

    while i < max_iterations:
        delta_w = -learning_rate * grad_func(noisy_cameraman,w)
        w = w + delta_w
        # update iteration number and diff between successive values
        # of objective function
        err = mean_squared_error(cameraman_grey, w)
        print("MSE at iteration",i,"is", err)
        mse.append(err)
        i += 1
    return w

result1=gradient_descent(30, img1, 0.001)
#result2=gradient_descent(30, img2, 0.001)
#result3=gradient_descent(30, img3, 0.001)

fig=plt.figure()
"""
i = 1
image = [img1, img2, img3]
result = [result1, result2, result3]
for i in range(6):
    fig.add_subplot(2, 3, i)
    plt.imshow(image(i), cmap=plt.cm.gray)  # displaying blur cameraman corrupted 20%
    plt.title("Image corrupted")
    plt.axis('off')

    fig.add_subplot(2, 3, i+3)
    plt.imshow(result(i), cmap=plt.cm.gray)  # displaying corrected image
    plt.title("Corrected image")
    plt.axis('off')
    i = i+1

"""
fig.add_subplot(1,2, 1)
plt.imshow(img1, cmap=plt.cm.gray)      # displaying blur cameraman corrupted 20%
plt.title("Image corrupted by 20%")
plt.axis('off')

fig.add_subplot(1,2,2)
plt.imshow(result1, cmap=plt.cm.gray)      # displaying corrected image
plt.title("Corrected image")
plt.axis('off')
plt.show()


k_list = range(1,31,1)
plt.plot(k_list,mse)
plt.xlabel("number of iterations")
plt.ylabel("mse")
plt.show()




